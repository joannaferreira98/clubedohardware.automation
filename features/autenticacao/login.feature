#language:pt
Funcionalidade: Autenticação

     @login
     Cenario: Login
         Dado que esteja na página inicial
         Quando acessar o login como "teste@gmail.com" e "senhateste"
         Então deverá ser exibida a mensagem de boas-vindas "Olá"