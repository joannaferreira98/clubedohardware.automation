#language:pt
Funcionalidade: Autenticação

     @esqueci_senha
     Cenario: Esqueci senha
           Dado que esteja na página de login
           Quando acessar esqueceu sua senha
           E informar o e-mail "teste@email.com"
           Então deverá exibir a mensagem "Enviamos um email para teste@email.com"