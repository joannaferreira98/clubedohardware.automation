#language:pt
Funcionalidade: Acesso VIP

     @Acesso_Vip
     Cenario: Clube do Hardware como membro VIP
         Dado que esteja na página inicial
         Quando acessar o Acesso VIP
         E passar os dados do cliente
         Então deverá ser exibida a página de checkout